#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

##tests
#epicsEnvSet("IOCBL", "XNS")
#epicsEnvSet("IOCDEV", "EIGER")
#epicsEnvSet("DEVIP", "172.17.10.245")

##Macros
epicsEnvSet("PREFIX", "$(IOCBL):$(IOCDEV):")
epicsEnvSet("PORT",   "EIG")
epicsEnvSet("QSIZE",  "20")
epicsEnvSet("XSIZE",  "1030")
epicsEnvSet("YSIZE",  "514")
epicsEnvSet("NCHANS", "2048")
epicsEnvSet("CBUFFS", "500")
epicsEnvSet("MAX_THREADS", "8")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db:$(ADEIGER)/db")

# The datatype of the waveform record
# STRING,CHAR,UCHAR,SHORT,USHORT,LONG,ULONG,INT64,UINT64,FLOAT,DOUBLE,ENUM
epicsEnvSet("FTVL", "LONG")
# The asyn interface waveform record
# Int8, Int16, Int32, Int64, Float32 or Float64
epicsEnvSet("TYPE", "Int32")

## 1030*514 = 529420 * 4(bytes) = 2117680
epicsEnvSet("NELEMENTS", "529420")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "2120000")


## Load record instances
eigerDetectorConfig("$(PORT)", "$(DEVIP)", 0, 0)
dbLoadRecords("$(ADEIGER)/db/eiger2.template", "P=$(PREFIX),R=cam1:,PORT=$(PORT),ADDR=0,TIMEOUT=1")

## extra custom support records
dbLoadRecords("${TOP}/iocBoot/${IOC}/eigersup.db", "BL=$(IOCBL), DEV=$(IOCDEV), DEV2=image3")

# Create standard arrays plugins
NDStdArraysConfigure("Image1", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS), NDARRAY_PORT=$(PORT)")
NDStdArraysConfigure("Image2", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS), NDARRAY_PORT=$(PORT)")
NDStdArraysConfigure("Image3", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image3:,PORT=Image3,ADDR=0,TIMEOUT=1,TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS), NDARRAY_PORT=$(PORT)")
NDStdArraysConfigure("Image4", 5, 0, "$(PORT)", 0, 0)
dbLoadRecords("$(ADCORE)/db/NDStdArrays.template", "P=$(PREFIX),R=image4:,PORT=Image4,ADDR=0,TIMEOUT=1,TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS), NDARRAY_PORT=$(PORT)")

## load common plugins
< ${TOP}/iocBoot/${IOC}/commonPlugins.cmd

cd "${TOP}/iocBoot/${IOC}"

## autosave
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_requestfile_path("$(ADEIGER)/eigerApp/Db")
set_requestfile_path("$(CALC)/calcApp/Db")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX),BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

epicsThreadSleep 2

dbpf $(PREFIX)cam1:DataSource 2
dbpf $(PREFIX)cam1:ReadStatus.SCAN "1 second"
